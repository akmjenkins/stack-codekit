IMPORTANT
==========

1. Modify bower_components/fontawesome/scss/_variables.scss

Comment out (//) line 4 and uncomment line 5 so FontAwesome font files will be pulled from the bootstrap CDN
**OR**
Change line 4 so the path points to ../../bower_components/fontawesome/fonts/ instead of ../font

2. TypeKit

The font "Proxima Nova" was used in this template, but is only available from TypeKit.
Go to TypeKit, create a new kit with the correct domain name(s) and add the Proxima Nova font to it.
The weights needed are simply "Regular" and "Bold" (and the italic versions of both)
Put the JS code - i.e. 

`<script src="//use.typekit.net/gns1hzo.js"></script>
<script>try{Typekit.load();}catch(e){}</script>`

in the inc/i-header.php file
