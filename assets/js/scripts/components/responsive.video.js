(function(context) {
	//responsive video
	$('iframe.video,iframe[src*="youtube"],iframe[src*="vimeo"]').each(function() { 
		$(this).removeAttr('width').removeAttr('height').wrap('<div class="video-container"/>') 
	});
}(window[ns]));