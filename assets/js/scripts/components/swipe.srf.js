(function(context) {

	var 
		Swipe = context.Swipe,
		SimpleResponsiveFader = context.SimpleResponsiveFader,
		buildSwipe,
		buildSRF;
		
	//SRF
	(function() {
		buildSRF = function() {
			//if a touch enabled device, use a swiper instead
			if(Modernizr.touch && !$(this).hasClass('force-srf')) {
				$(this)
					.removeClass('srf')
					.addClass('swipe')
					.children()
					.removeClass('slider')
					.addClass('swipe-wrap')
				return;
			}
		
				var 
					el = $(this);
					
				el
					.children()
					.addClass('slider')
					.end()
					.SimpleResponsiveFader({
						delay			:	( (+el.data('auto')*1000) || 5000),
						speed			:	500,
						resizeHeight	:	false,
						resizeHeightSpeed: 500,
						fadeType		:	el.data('type') || 'cross',
						pauseOnHover : !!(el.data('pauseonhover')),
						buildLinks		: 	!!(el.data('controls')),
						centerLinks	: 	false,
						buildControls	:	!!(el.data('links'))
					});
					
					if(!el.data('auto')) { el.SimpleResponsiveFader().pause(); }
		};
		
		$('div.srf').each(function() { buildSRF.apply(this,arguments); });
	}());
	
	//swiper
	(function() {
		buildSwipe = function() {
			var 
				swipe,
				el = $(this),
				//links = $(this).find('div.links a'),
				children = el.children('div.swipe-wrap').children('div');
				
				if(el.data('swipe')) { return; }
				
				if(children.length == 1) {
					el.css('visibility','visible');
				} else {
				
					swipe = new Swipe(el[0],{
						continuous: (el.data('notcontinuous') !== undefined) ? el.data('notcontinuous') : true,
						auto:( (+el.data('auto')*1000) || false),
						startSlide: (el.data('start')) || 0,
						stopOnClick:  (el.data('stoponclick') !== undefined) ? el.data('stoponclick') : true,
						speed: (+el.data('speed')) || 500,
						callback: function(pos,div) {
							var 
								i,
								d = $(div).data();
								
								if(d) {
									i = (typeof d.originalindex === 'undefined') ? d.index : d.originalindex;
								} else {
									i = pos;
									//return;
								}
							
							$('div.swipe',el).each(function() { buildSwipe.apply(this,arguments); });
						
							el.trigger('swipeChanged',[i,div]);
							if(el.data('controls') || el.data('controlbar')) {	
								el.parent().find('div.slider-controls a,div.slider-controls-links a.item').eq(i).addClass('selected').siblings().removeClass('selected');
							}
							
						}
					});
				
				}
				
				if(el.closest('div.swiper-wrapper').hasClass('paginated-swiper')) {
					(function() {
					
						if(children.length <= 1) {
							el.closest('div.swiper-wrapper').addClass('hide-links');
							return;
						}
					
						var
							pel = el.closest('div.swiper-wrapper'),
							paginationEl = $('div.pagination',pel),
							total = $('span.total',paginationEl),
							current = $('span.current',paginationEl),
							swiperEl = pel.children('div.swipe'),
							swiper = swiperEl.data('swipe'),
							perSlide = $('div.grid',pel).eq(0).children().length,
							totalCount = parseInt(total.html(),10);
							
							paginationEl.removeClass('hide');
							
							swiperEl.on('swipeChanged',function(e,i,div) {
								current.html('Page '+(i+1)+' of '+children.length);
							});
							
							
					}());
				}
				
				el.children('div.swipe-wrap').children('div').each(function(i,el) {
					if(i > children.length-1) {
						$(this).data('originalindex',((i%children.length))).addClass('ghost');
					}
				});
				
				el.data('swipe',swipe);
				
			if(children.length > 1) {
			
				if(el.data('controlbar')) {
				
					var controlString = '<div class="slider-controls-links swiper-nav-bar pagination"><div><a class="previous fa" title="Previous">Previous</a>';
					children.each(function(i,el) {
						controlString += '<a class="item '+((!i) ? 'selected' : '')+'">'+(i+1)+'</a>';
					});
					controlString += '<a class="next fa" title="Next">Next</a></div></div>';
					el
						.parent()
						.append(controlString)
						.on('click','div.slider-controls-links a',function(e) {
							e.preventDefault();
							var el = $(this);
							
							e.preventDefault();
							
							if(el.hasClass('next')) {
								swipe.next();
								return;
							}
							
							if(el.hasClass('prev')) {
								swipe.prev();
								return;
							}
							
							swipe.slide(el.parent().children('a.item').index(el));
						});
						
					return;
				
				}

			
				if(el.data('controls')) {
					
					var controlString = '<div class="slider-controls"><div>';
					children.each(function(i,el) {
						controlString += '<a class="'+((!i) ? 'selected' : '')+'">'+(i+1)+'</a>';
					});
					controlString += '</div></div>';
					el
						.parent()
						.append(controlString);
					
					
				}
			
				if(el.data('links')) {
					el
						.parent()
						.append('<div class="links"><a class="previous fa" title="Previous">Previous</a><a class="next fa" title="Next">Next</a></div>');
				}
				
				//add the listeners
					el
						.parent()
						.on('click','div.slider-controls a',function(e) {
							e.preventDefault();
							swipe.slide($(this).index());
							
							//restart, if required
							
							
						})
						.on('click','div.links a',function(e) {
							
							e.preventDefault();
							var el = $(this);
							if(el.hasClass('next')) {
								swipe.next();
							} else {
								swipe.prev();
							}
						});
			}
		};

		$('div.swipe').each(function() { buildSwipe.apply(this,arguments); });
	}());


	$.extend(context,{
		SwipeSRF: {
			buildSwipe: buildSwipe,
			buildSRF: buildSRF					
		}
	});

}(window[ns]));