(function(context){

	var 
		scrollDebounce = context.debounce(),
		resizeDebounce = context.debounce(),	
		$document = $(document),
		$window = $(window),
		$html = $('html'),
		$nav = $('nav'),
		$body = $('body'),
		SHOW_CLASS = 'show-nav',
		SMALL_NAV = 'small-nav',
		startingNavPosition = 160;

	//mobile nav
	$document
		.on('click','.toggle-nav',function(e) {
			methods.toggleNav();
			return false;
		})
		.on('click','body',function(e) {
			$(e.target).hasClass('mobile-nav-bg') && methods.isShowingNav() && methods.showNav(false);
		})
		.on('keyup',function(e) {
			e.keyCode === 27 && methods.isShowingNav() && methods.toggleNav();
		});

	var methods = {

		checkShowSmallNav: function() {
			
			if($window.innerWidth() > 750 && $window.scrollTop() > startingNavPosition) {
				$body.addClass(SMALL_NAV);
			} else {
				$body.removeClass(SMALL_NAV);
			}
		},

		onScroll: function() {
			this.checkShowSmallNav();
		},
		
		onResize: function() {
			this.checkShowSmallNav();
			$nav.find('div.swipe').removeClass('rebuilt');
		},

		showNav: function(show) {
			var method = 'removeClass';
			
			if(show) {
				method = 'addClass';
				setTimeout(function() {
					$nav.find('a').eq(0).focus();
				},400);
			}
			
			$html[method](SHOW_CLASS);

		},

		toggleNav: function() {
			this.showNav(!this.isShowingNav());
		},

		isShowingNav: function() {
			return $html.hasClass(SHOW_CLASS);
		}

	};

	$window
		.on('scroll',function() {
			scrollDebounce.requestProcess(methods.onScroll,methods);
		})
		.on('resize',function() {
			resizeDebounce.requestProcess(methods.onResize,methods);
		});
		
		if(Modernizr.ios) {
			
			//onScroll must be fired continuously
			(function iOSOnScroll() {
				methods.onScroll();
				requestAnimationFrame(function() { iOSOnScroll(); });
			}());
		
		} else {
			//fire immediately
			methods.onScroll();
			methods.onResize();
		}	

}(window[ns]));
