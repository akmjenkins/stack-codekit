var ns = 'STACK';
window[ns] = {};

// debounce - used in lots of stuff
// @codekit-append "scripts/debounce.js";

// @codekit-append "scripts/components/anchors.external.popup.js"; 
// @codekit-append "scripts/components/standard.accordion.js"
// @codekit-append "scripts/components/custom.select.js";

// @codekit-append "scripts/components/responsive.video.js"; 

// Used in swipe.srf.js
// @codekit-append "scripts/components/swipe.js"; 
// @codekit-append "scripts/components/srf.js"; 
// @codekit-append "scripts/components/swipe.srf.js"; 

// @codekit-append "scripts/search.js"; 
// @codekit-append "scripts/hero.js"; 
// @codekit-append "scripts/nav.js";

// Additional Modernizr Tests
// @codekit-append "modernizr_tests/ios.js";
// @codekit-append "modernizr_tests/android.js";


(function(context) {
	
		var $window = $(window);
		
		
		(function() {
			$('.video-container').each(function() {
				var 
					el = $(this),
					ratio = el.data('ratio');
					
					el.css({paddingBottom:ratio+'%'});
			});
		}());
		
		//overview blocks
		(function() {
			var
				$blocks = $('.overview-block'),
				windowHeight = $window.innerHeight(),
				updateHeight = false,
				rafDebounce = {
				
					isProcessing: false,
					method: null,
					methodScope:null,
					
					requestProcess: function(method,scope) {
						if(!this.isProcessing) {
							this.method = method;
							this.methodScope = scope || window;
							this.isProcessing = true;
							requestAnimationFrame(this.process.bind(this));
						}
					},
					
					process: function() {
						this.method.apply(this.methodScope);
						this.method = null;
						this.methodScope = null
						this.isProcessing = false;
					}
					
				},
				methods = {
				
					updateBlockImages: function() {
						if(updateHeight) {
							windowHeight = $window.innerHeight();
						}
						var topOfBlock = $window.scrollTop()+(windowHeight/3);
						
						this.loadImageForBlocks($blocks.filter(function() {
								var el = $(this);
								return !el.hasClass('loading') && !el.hasClass('loaded') && el.offset().top-topOfBlock < 0
							})
						);
						
					},
				
					loadImageForBlocks: function(blocks) {
					
						$.each(blocks,function() {
							var 
								el = $(this),
								img = $('div.img',el),
								source = img.data('src');
								
							el.addClass('loading');
							
							$('<img/>')
								.on('load error',function() {
									el.removeClass('loading').addClass('loaded');
									img.css({backgroundImage:'url('+source+')'});
								})
								.attr('src',source)
								
						});
						
					},
				};
			
			$blocks.length && $window.on('scroll load resize',function(e) { 
				upateHeight = e.type === 'resize';
				rafDebounce.requestProcess(methods.updateBlockImages,methods); 
			});
		}());

		$(document)
			.on('click','.inline-video',function(e) {
				var 
					el = $(this),
					src = el.data('src');
					
				if(src.length) {
					el
						.removeClass('inline-video')
						.addClass('inline-video-playing')
						.append('<iframe src="'+src+'"/>');
				}
				
			});		

}(window[ns]));