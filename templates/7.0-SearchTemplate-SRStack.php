<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-1.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<section class="dark-bg page-header">
		<div class="sw">
			<h1>Search Results</h1>
			<span class="h3-style subtitle">10 Results found for "Query"</span>
		</div><!-- .sw -->
	</section><!-- .page-header -->
	
	<section class="filter-area nopad">
		<div class="filter-bar">
			<div class="sw">
			
				<div class="filter-controls">
					<button class="previous">Prev</button>
					<button class="next">Next</button>
				</div><!-- .filter-arrows -->
			
				<div class="count">
					<span class="num">3</span>
					News Results Found
				</div><!-- .count -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
			<div class="sw">
				<div class="grid eqh">
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">S.R. Stack Becomes Platinum Sponsor of the Manulife Relay for Life Fundraiser.</h5>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta eleifend nunc, et sagittis augue porttitor sed. 
								Sed pharetra erat non lacus venenatis, sit amet ultricies erat porttitor. Nulla ullamcorper odio ac euismod pellentesque. 
								Vestibulum eget nisl ut lorem maximus lobortis.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">A shorter title.</h5>
							
							<p>
								With hardly any content - and yet the blocks still line up reasonably well
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">S.R. Stack Becomes Platinum Sponsor of the Manulife Relay for Life Fundraiser.</h5>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta eleifend nunc, et sagittis augue porttitor sed. 
								Sed pharetra erat non lacus venenatis, sit amet ultricies erat porttitor. Nulla ullamcorper odio ac euismod pellentesque. 
								Vestibulum eget nisl ut lorem maximus lobortis.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->

				</div><!-- .grid -->
			</div><!-- .sw -->
		</div><!-- .filter-content -->
		
	</section><!-- .filter-section -->

	<section class="filter-area nopad">
		<div class="filter-bar">
			<div class="sw">
			
				<div class="filter-controls">
					<button class="previous">Prev</button>
					<button class="next">Next</button>
				</div><!-- .filter-arrows -->
			
				<div class="count">
					<span class="num">2</span>
					Page Results Found
				</div><!-- .count -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
			<div class="sw">
				<div class="grid eqh">
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">S.R. Stack Becomes Platinum Sponsor of the Manulife Relay for Life Fundraiser.</h5>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta eleifend nunc, et sagittis augue porttitor sed. 
								Sed pharetra erat non lacus venenatis, sit amet ultricies erat porttitor. Nulla ullamcorper odio ac euismod pellentesque. 
								Vestibulum eget nisl ut lorem maximus lobortis.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">A shorter title.</h5>
							
							<p>
								With hardly any content - and yet the blocks still line up reasonably well
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			</div><!-- .sw -->
		</div><!-- .filter-content -->
		
	</section><!-- .filter-section -->
	
	<?php include('inc/i-pre-footer-modules.php'); ?>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>