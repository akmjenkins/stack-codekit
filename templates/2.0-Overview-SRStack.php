<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-1.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">
	
	<article>
		<section class="dark-bg page-header">
			<div class="sw">
				<h1>Get Help</h1>
				<span class="h3-style subtitle">Phasellus interdum tempus nisi quis placerat liquam mollis</span>
			</div><!-- .sw -->
		</section><!-- .page-header -->
	
		<div class="article-body">
			<section>
				
				<div class="sw">
					<div class="main-body with-big-sidebar">
						<div class="content">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum bibendum leo, ac dignissim orci cursus at. Donec in iaculis enim. Ut nibh nisl, 
								elementum nec tortor eu, ultrices pharetra purus. Nunc aliquam lacus enim. Ut suscipit nisi vitae metus suscipit convallis. Nunc in malesuada nibh, a 
								interdum dolor. Pellentesque justo leo, posuere eget ipsum at, elementum interdum sapien.
							</p><!-- .excerpt -->
						</div>
						<aside class="sidebar big-sidebar">
							<div class="contact-module">
							
								<span>Talk to us Today!</span>
							
								<div class="btn-group fa-buttons">
								
									<span class="fa-button fa fa-phone">
										<span class="block">709 722 5741</span>
									</span>
									
									<a class="fa-button fa fa-envelope-o" href="#">
										<span class="block">Email</span>
									</a>
									
									<a class="fa-button fa fa-comments-o" href="#">
										<span class="block">Live Chat</span>
									</a>
								
								</div><!-- .btn-group -->
							</div><!-- .contact-module -->
						</aside><!-- .sidebar -->
					</div><!-- .main-body -->
				</div><!-- .sw.cf -->
				
			</section>
		</div><!-- .article-body -->
	</article>
	
	<section class="dark-bg with-embellishment">
		<div class="sw">
		
			<div class="grid eqh">
				<div class="col-3 col sm-col-2 xs-col-1">
					<a class="item overview-block with-btn" href="#">
						
						<div class="img-wrap">
							<div class="img" data-src="../assets/images/temp/head-1.jpg"></div>
						</div><!-- .img-wrap -->
						
						<div class="hgroup">
							<h3>What to Expect</h3>
							<span class="subtitle">Subtitle</span>
						</div><!-- .hgroup -->
						
						<p>Integer et tortor eget justo laoreet auctor. Proin iaculis semper mauris sit amet lobortis. Quisque ullamcorper congue ornare. 
						Suspendisse odio elit, posuere nec urna id, lacinia efficitur felis</p>
						
						<div class="btn-wrap">
							<span class="button">Read More</span>
						</div><!-- .btn-wrap -->
						
					</a><!-- .item -->
				</div><!-- .col-3 -->
				<div class="col-3 col sm-col-2 xs-col-1">
					<a class="item overview-block with-btn" href="#">
						
						<div class="img-wrap">
							<div class="img" data-src="../assets/images/temp/head-2.jpg"></div>
						</div><!-- .img-wrap -->
						
						<div class="hgroup">
							<h3>Resources</h3>
							<span class="subtitle">Overview block with a long subtitle</span>
						</div><!-- .hgroup -->
						
						<p>Integer et tortor eget justo laoreet auctor. Proin iaculis semper mauris sit amet lobortis.</p>
						
						<div class="btn-wrap">
							<span class="button">Read More</span>
						</div><!-- .btn-wrap -->
						
					</a><!-- .item -->
				</div><!-- .col-3 -->
				<div class="col-3 col sm-col-2 xs-col-1">
					<a class="item overview-block with-btn" href="#">
						
						<div class="img-wrap">
							<div class="img" data-src="../assets/images/temp/head-3.jpg"></div>
						</div><!-- .img-wrap -->
						
						<div class="hgroup">
							<h3>Who We Are</h3>
							<span class="subtitle">Subtitle</span>
						</div><!-- .hgroup -->
						
						<p>Integer et tortor eget justo laoreet auctor. Proin iaculis semper mauris sit amet lobortis. Quisque ullamcorper congue ornare. 
						Suspendisse odio elit, posuere nec urna id, lacinia efficitur felis</p>
						
						<div class="btn-wrap">
							<span class="button">Read More</span>
						</div><!-- .btn-wrap -->
						
					</a><!-- .item -->
				</div><!-- .col-3 -->


			</div><!-- .grid.eqh -->
		
		</div><!-- .sw -->
	</section><!-- .with-embellishment -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>