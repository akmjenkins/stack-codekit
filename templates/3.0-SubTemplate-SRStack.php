<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-1.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">
	
	<article>
		<section class="dark-bg page-header">
			<div class="sw">
				<h1>Get Help</h1>
				<span class="h3-style subtitle">Phasellus interdum tempus nisi quis placerat liquam mollis</span>
			</div><!-- .sw -->
		</section><!-- .page-header -->
	
		<section>
			
			<div class="sw">
				<div class="main-body with-big-sidebar">
					<div class="content">
						<div class="article-body">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum bibendum leo, ac dignissim orci cursus at. Donec in iaculis enim. Ut nibh nisl, 
								elementum nec tortor eu, ultrices pharetra purus. Nunc aliquam lacus enim. Ut suscipit nisi vitae metus suscipit convallis. Nunc in malesuada nibh, a 
								interdum dolor. Pellentesque justo leo, posuere eget ipsum at, elementum interdum sapien.
							</p><!-- .excerpt -->
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar big-sidebar">
						<div class="contact-module">
						
							<span>Talk to us Today!</span>
						
							<div class="btn-group fa-buttons">
							
								<span class="fa-button fa fa-phone">
									<span class="block">709 722 5741</span>
								</span>
								
								<a class="fa-button fa fa-envelope-o" href="#">
									<span class="block">Email</span>
								</a>
								
								<a class="fa-button fa fa-comments-o" href="#">
									<span class="block">Live Chat</span>
								</a>
							
							</div><!-- .btn-group -->
						</div><!-- .contact-module -->
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</div><!-- .sw -->
			
		</section>
		
		<section class="with-embellishment">
			<div class="sw">
				
				<div class="breadcrumbs">
					<a href="#">Get Help</a>
					<a href="#">What to Expect</a>
				</div><!-- .breadcrumbs -->
				
				<div class="main-body with-sidebar">
					<div class="content">
						<div class="article-body">
						
							<h2>Sub Header (h2)</h2>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
								Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula. Donec lorem orci, 
								semper et mi at, sagittis accumsan felis. Curabitur mollis justo eu felis laoreet viverra. Vestibulum blandit quis libero vitae consectetur. Nullam consequat 
								velit eu lacus faucibus posuere. Aenean facilisis, ex vitae euismod iaculis, massa risus ornare dui, quis tincidunt tellus metus tristique libero. 
								Suspendisse eleifend cursus urna, eu congue ante vestibulum et. Praesent iaculis maximus suscipit. Sed eleifend malesuada accumsan. Cum 
								sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In ultrices pulvinar eleifend. Vestibulum consectetur id urna sit amet 
								gravida. Proin et rutrum sem. Aliquam a iaculis ex. Nulla tincidunt eget nisl at consectetur. Suspendisse rhoncus mi nec nibh ullamcorper fringilla. 
								Vivamus ultricies eu dui et aliquam. Duis non tincidunt lectus, in bibendum justo. Mauris ullamcorper eros in libero pulvinar mattis. Aliquam ultricies 
								laoreet lorem dictum vestibulum.
							</p>
							
							<h3>Sub Header (h3)</h3>
							
							<p>
								Pellentesque porta luctus ornare. Etiam elementum nisl libero, vel auctor nunc feugiat ut. Duis eu sodales nunc. Proin id massa commodo, laoreet urna eu, 
								maximus lectus. Fusce sed magna venenatis, pharetra lorem non, sodales turpis. Phasellus dapibus metus eu neque interdum, nec sollicitudin ipsum interdum. 
								Integer et tortor eget justo laoreet auctor. Proin iaculis semper mauris sit amet lobortis. Quisque ullamcorper congue ornare. Suspendisse odio elit, posuere 
								nec urna id, lacinia efficitur felis. Donec ultrices, quam eget mattis venenatis, odio sem tristique lacus, vel mollis purus dui eget purus. Quisque tellus 
								ipsum, tempor et metus in, commodo rhoncus lectus.
							</p>
							
							<p>
								Pellentesque porta luctus ornare. Etiam elementum nisl libero, vel auctor nunc feugiat ut. Duis eu sodales nunc. Proin id massa commodo, laoreet urna eu, 
								maximus lectus. Fusce sed magna venenatis, pharetra lorem non, sodales turpis. Phasellus dapibus metus eu neque interdum, nec sollicitudin ipsum interdum. 
								Integer et tortor eget justo laoreet auctor. Proin iaculis semper mauris sit amet lobortis. Quisque ullamcorper congue ornare. Suspendisse odio elit, posuere 
								nec urna id, lacinia efficitur felis. Donec ultrices, quam eget mattis venenatis, odio sem tristique lacus, vel mollis purus dui eget purus. Quisque tellus 
								ipsum, tempor et metus in, commodo rhoncus lectus.
							</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar padded-blocks">
						<div class="dark-bg in-this-section">
							<h4 class="title pad">In This Section</h4>
							
							<ul>
								<li class="selected"><a class="fa fa-abs" href="#">What to Expect</a></li>
								<li><a class="fa fa-abs" href="#">Resources</a></li>
								<li><a class="fa fa-abs" href="#">Who We Are</a></li>
								<li><a class="fa fa-abs" href="#">Talk to us Today</a></li>
							</ul>
							
						</div><!-- .dark-bg -->
						
						<?php include('inc/i-did-you-know-sidebar.php'); ?>						
					</aside><!-- .sidebar -->
				</div><!-- .main-body.with-sidebar -->
				
			</div><!-- .sw -->
		</section><!-- .with-embellishment -->
			

	</article>
	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>