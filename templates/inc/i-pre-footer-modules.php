	<section class="dark-bg pre-footer-modules">
		<div class="sw">
		
			<div class="grid eqh collapse-850">
				<div class="col-3-5 col">
					<div class="item did-you-know">
						
						<h3>Did You Know?</h3>
						
						<div class="swiper-wrapper">
						
							<div class="swipe" data-links="true">
								<div class="swipe-wrap">
									<div>
										<ol>
											<li>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
											</li>
											<li>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
											</li>
										</ol>
									</div>
									<div>
										<ol>
											<li>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
											</li>
											<li>
												Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla tempus, mauris quis sodales.
											</li>
										</ol>
									</div>
								</div><!-- .swipe-wrap -->
							</div><!-- .swipe -->
						</div><!-- .swiper-wrapper -->

						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col-2-5 col">
					<div class="item">
						
						<h3>Subscribe</h3>
						
						<div class="green-bg subscribe-block">
							<p>Enter your e-mail address to sign up for e-mail updates</p>
							
							<form action="/" method="post" class="single-form">
								<fieldset>
									<input type="email" name="subscribe" placeholder="Email address...">
									<button class="fa-paper-plane-o">Subscribe</button>
								</fieldset>
							</form><!-- .single-form -->
						</div><!-- .green-bg -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section><!-- .dark-bg -->
