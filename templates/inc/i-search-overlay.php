			<div class="search-overlay no-results">
				
				
				<div class="search-content">
					
					<form action="/" method="get" class="search-form">
						<input type="text" name="search" placeholder="Search S.R. Stack &amp; Company">
						<button type="button" class="toggle-search-form close fa fa-times fa-abs">Close</button>
					</form><!-- .search-form -->
					
				</div><!-- .search-content -->
				
			</div><!-- .search-overlay -->