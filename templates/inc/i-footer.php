			<footer>
				<div class="sw">
				
					<div class="footer-blocks">
						
						<div class="footer-block">
							<a href="#" class="footer-logo">
								<img src="../assets/images/sr-stack-logo-white.svg" alt="S.R. Stack White Logo">
							</a>
							
							<address>
								84-86 Elizabeth Ave. <br />
								Regatta Plaza 2 <br />
								St. John's, NL A1A 1W7
							</address>
						</div>
						
						<div class="footer-block">
							
							<div class="phones rows">
							
								<span class="row">
									<span class="l">St. John's Area:</span>
									709 722 5741
								</span><!-- .block -->
								
								<span class="row">
									<span class="l">Toll Free:</span>
									1 800 285 8870
								</span><!-- .block -->
								
								<span class="row">
									<span class="l">After Hours:</span>
									709 834 4190
								</span><!-- .block -->
							</div><!-- .phones -->
							
						</div>
						
						<div class="footer-block footer-nav">
							
							<ul>
								<li><a href="#">Get Help</a></li>
								<li><a href="#">Free Consulations</a></li>
								<li><a href="#">Who We Can Help</a></li>
								<li><a href="#">The Latest</a></li>
								<li><a href="#">Solutions</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
							
						</div>
						
					</div><!-- .footer-blocks -->
					
					<div class="copyright">
					
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">S.R. Stack &amp; Company Ltd.</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
							<li>All Photographs Copyright R.P. Stack <?php echo date('Y'); ?></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/images/jac-logo.svg" alt="JAC. We Create."></a>
					</div><!-- .copyright -->
					
				</div><!-- .sw -->
			
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/srstack',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/js/min/main-min.js"></script>

		<?php include('i-search-overlay.php'); ?>
	</body>
</html>
