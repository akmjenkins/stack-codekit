<div class="mobile-nav-bg"></div>
<div class="nav-wrap">

	<div class="nav">
		<div class="sw">
		
			<nav>
				<ul>
				
					<li>
						<a href="#">Get Help</a>
						<div>
							<ul>
								<li><a href="#">What to Expect</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Who We Are</a></li>
								<li><a href="#">Talk to Us Today</a></li>
							</ul>
						</div>
						
					</li>
					
					<li>
						<a href="#">Who We Help</a>
						<div>
							<ul>
								<li><a href="#">What to Expect</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Who We Are</a></li>
								<li><a href="#">Talk to Us Today</a></li>
							</ul>
						</div>
						
					</li>
					
					<li>
						<a href="#">Solutions</a>
						<div>
							<ul>
								<li><a href="#">What to Expect</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Who We Are</a></li>
								<li><a href="#">Talk to Us Today</a></li>
							</ul>
						</div>
						
					</li>
					
					<li>
						<a href="#">Free Consultations</a>
						<div>
							<ul>
								<li><a href="#">What to Expect</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Who We Are</a></li>
								<li><a href="#">Talk to Us Today</a></li>
							</ul>
						</div>
						
					</li>
					
					<li>
						<a href="#">The Latest</a>
						<div>
							<ul>
								<li><a href="#">What to Expect</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Who We Are</a></li>
								<li><a href="#">Talk to Us Today</a></li>
							</ul>
						</div>
						
					</li>
					
				</ul>
			</nav>
		

			<div class="nav-top">
			
				<button id="toggle-search" class="toggle-search-form fa-abs">Search</button>
				
				<div class="social-icons">
					<a href="#" rel="external" class="fa fa-abs fa-linkedin">Connect with S.R. Stack on LinkedIn</a>
					<a href="#" rel="external" class="fa fa-abs fa-rss">Stay up to date with S.R. Stack via RSS</a>
				</div><!-- .social -->
			
				<div class="nav-meta-nav">
					<a href="#">Resources</a>
					<a href="#">Contact</a>
				</div><!-- .meta-nav -->

			</div><!-- .top -->
			
		</div><!-- .sw -->
	</div><!-- .nav -->
</div><!-- .nav-wrap -->