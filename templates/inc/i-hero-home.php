<div class="hero">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/hero-1.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<span class="title h1-style">Debt Counselling</span>
							
							<p>
								We can help with Debt Counselling, Consumer Proposals or Bankruptcy. Debt Solutions made in Newfoundland and Labrador
							</p>
							
							<a href="#" class="button big">Read More</a>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>
				
				<div data-src="../assets/images/temp/hero/hero-1-i.jpg">
					<div class="item">&nbsp;</div>
					
					<div class="caption">
						<div class="sw">
						
							<span class="title h1-style">Lorem Ipsum</span>
							
							<p>
								We can help with Debt Counselling, Consumer Proposals or Bankruptcy. Debt Solutions made in Newfoundland and Labrador
							</p>
							
							<a href="#" class="button big">Read More</a>

						</div><!-- .sw -->
					</div><!-- .caption -->
					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->