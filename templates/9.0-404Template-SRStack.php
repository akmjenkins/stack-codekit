<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-2.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">
	
	<section class="dark-bg page-header">
		<div class="sw">
			<h1>404 Error</h1>
			<span class="h3-style subtitle">Phasellus interdum tempus nisi quis placerat liquam mollis</span>
		</div><!-- .sw -->
	</section><!-- .page-header -->
	
	<section>
		<div class="sw">
		
			<div class="grid centered">
				<div class="col col-3-5 sm-col-1">
					<div class="item center">
						
						<span class="h4-style">
							The page you're looking for cannot be found. Please use the buttons below to navigate back to the page you were previously viewing,
							or search the site for what you are looking for.
						</span>
						
						<div class="error404-box dark-bg">
							
							<div class="btn-group">
								<a href="#" class="button big">Go Back</a>
								<a href="#" class="button big">Contact Us</a>
							</div><!-- .btn-group -->
							
							<form action="/" method="get" class="single-form">
								<fieldset>
									<input type="text" name="s" placeholder="Type your search here...">
									<button class="fa-search">Search</button>
								</fieldset>
							</form>
							
						</div><!-- .error404-box -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid.centered -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="with-embellishment">
		
		<div class="sw">
			<div class="main-body with-big-sidebar">
				<div class="content">
					<div class="article-body">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum bibendum leo, ac dignissim orci cursus at. Donec in iaculis enim. Ut nibh nisl, 
							elementum nec tortor eu, ultrices pharetra purus. Nunc aliquam lacus enim. Ut suscipit nisi vitae metus suscipit convallis. Nunc in malesuada nibh, a 
							interdum dolor. Pellentesque justo leo, posuere eget ipsum at, elementum interdum sapien.
						</p><!-- .excerpt -->
					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar big-sidebar">
					<div class="contact-module">
					
						<span>Talk to us Today!</span>
					
						<div class="btn-group fa-buttons">
						
							<span class="fa-button fa fa-phone">
								<span class="block">709 722 5741</span>
							</span>
							
							<a class="fa-button fa fa-envelope-o" href="#">
								<span class="block">Email</span>
							</a>
							
							<a class="fa-button fa fa-comments-o" href="#">
								<span class="block">Live Chat</span>
							</a>
						
						</div><!-- .btn-group -->
					</div><!-- .contact-module -->
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		</div><!-- .sw -->
		
	</section>			

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>