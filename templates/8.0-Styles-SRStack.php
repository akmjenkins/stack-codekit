<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe" data-controls="true" data-auto="7">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-1.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">
	
	<section class="dark-bg page-header">
		<div class="sw">
			<h1>Styles - 32px Proxima Nova Regular - #129278</h1>
			<span class="h3-style subtitle">Subtitle - 24px Proxima Nova Regular - #ffffff</span>
		</div><!-- .sw -->
	</section><!-- .page-header -->
	
	<div class="article-body">

		<section>

			<div class="sw">
				<div class="section-title">
					<h2>Typography</h2>
				</div><!-- .section-title -->
			</div><!-- .sw -->
			
			<hr class="embellishment" />
		
			<div class="sw">
			
				<div class="center">
					<h1>*H1 - 32px - Proxima Nova Regular - #129278</h1>
					<h2>H2 - 28px - Proxima Nova Regular - #129278</h2>
					<h3>H3 - 24px - Proxima Nova Regular - #129278</h2>
					<h4>H4 - 20px - Proxima Nova Regular - #129278</h2>
					<h5>H5 - 18px - Proxima Nova Bold - #129278</h2>
					<h6>H6 - 16px - Proxima Nova Bold - #129278</h2>
					
					<small>*There is no 36px header anywhere in the templates other than here, yet there are H1's elsewhere (like the title of this page)</small>
				</div><!-- .center -->
			
			
				<div class="grid collapse-850">
					<div class="col col-2">
						<div class="item">
							<p class="excerpt">
								Blockquote/excerpt - 18px Proxima Nova Regular - #25282F - 26px line height - Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Cras a velit ac risus blandit ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
								Sed rhoncus erat at purus iaculis suscipit
							</p><!-- .justify -->
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-2">
						<div class="item">
							<p>
								Paragraph - 16px Proxima Nova Regular - #25282F - 24px line height - Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
								Cras a velit ac risus blandit ultrices. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
								Sed rhoncus erat at purus iaculis suscipit
							</p><!-- .justify -->
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			
			</div><!-- .sw -->

		</section>
		
		<section>

			<div class="sw">
				<div class="section-title">
					<h2>Colour Scheme</h2>
				</div><!-- .section-title -->
			</div><!-- .sw -->
			
			<hr class="embellishment" />
			
		</section>
		
		<section>

			<div class="sw">
				<div class="section-title">
					<h2>Lists</h2>
				</div><!-- .section-title -->
			</div><!-- .sw -->
			
			<hr class="embellishment" />
			
			<div class="sw">
			
				<div class="grid pad40 collapse-850">
					<div class="col col-2">
						<div class="item">
						
							<h2>Unordered List</h2>
							
							<ul>
								<li>Donec ac augue eu magna malesuada fermentum vel lacinia ante. Maecenas risus mi, rutrum quis commodo eget, hendrerit quis tortor.</li>
								<li>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.
									<ul>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.</li>
									</ul>
								</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi. </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi. </li>
							</ul>
						
						</div><!-- .item -->
					</div><!-- .col -->
					<div class="col col-2">
						<div class="item">
						
							<h2>Ordered List</h2>
							
							<ol>
								<li>Donec ac augue eu magna malesuada fermentum vel lacinia ante. Maecenas risus mi, rutrum quis commodo eget, hendrerit quis tortor.</li>
								<li>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.
									<ol>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.</li>
										<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi.</li>
									</ol>
								</li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi. </li>
								<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer at blandit magna. Aliquam leo sapien, dictum at sem in, egestas dignissim nisi. </li>
							</ol>
						
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
			
			</div><!-- .sw -->
		</section>
		
		<section>

			<div class="sw">
				<div class="section-title">
					<h2>Icons</h2>
				</div><!-- .section-title -->
			</div><!-- .sw -->
			
			<hr class="embellishment" />
			
		</section>
		
		<section>

			<div class="sw">
				<div class="section-title">
					<h2>Link Colours</h2>
				</div><!-- .section-title -->
			</div><!-- .sw -->
			
			<hr class="embellishment" />
			
		</section>

	</div><!-- .article-body -->
	
	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>