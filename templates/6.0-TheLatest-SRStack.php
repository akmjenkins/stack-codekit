<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-1.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">
	
	<section class="dark-bg page-header">
		<div class="sw">
			<h1>Get Help</h1>
			<span class="h3-style subtitle">Phasellus interdum tempus nisi quis placerat liquam mollis</span>
		</div><!-- .sw -->
	</section><!-- .page-header -->

	<section>
		
		<div class="sw">
			<div class="main-body with-big-sidebar">
				<div class="content">
					<div class="article-body">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum bibendum leo, ac dignissim orci cursus at. Donec in iaculis enim. Ut nibh nisl, 
							elementum nec tortor eu, ultrices pharetra purus. Nunc aliquam lacus enim. Ut suscipit nisi vitae metus suscipit convallis. Nunc in malesuada nibh, a 
							interdum dolor. Pellentesque justo leo, posuere eget ipsum at, elementum interdum sapien.
						</p><!-- .excerpt -->
					</div><!-- .article-body -->
				</div><!-- .content -->
				<aside class="sidebar big-sidebar">
					<div class="contact-module">
					
						<span>Talk to us Today!</span>
					
						<div class="btn-group fa-buttons">
						
							<span class="fa-button fa fa-phone">
								<span class="block">709 722 5741</span>
							</span>
							
							<a class="fa-button fa fa-envelope-o" href="#">
								<span class="block">Email</span>
							</a>
							
							<a class="fa-button fa fa-comments-o" href="#">
								<span class="block">Live Chat</span>
							</a>
						
						</div><!-- .btn-group -->
					</div><!-- .contact-module -->
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
		</div><!-- .sw -->
		
	</section>
	
	<section class="with-embellishment dark-bg">
		<div class="sw">
			
			<div class="breadcrumbs">
				<a href="#">The Latest</a>
			</div><!-- .breadcrumbs -->
			
			<div class="latest-block">
				<div class="featured">
				
						<div class="article-body">
							<span class="tag">News Release</span>
							<time datetime="2014-05-29" class="block-time">
								<span class="day">29</span>
								<span class="month">May</span>
								<span class="year">2014</span>
							</time>
							
							<h3 class="title">Fusce ornare odio sed erat sagittus euismod.</h3>
							<p>
								Mauris urna est, sodales ac pulvinar in, pellentesque sed velit. Integer iaculis risus sed porta efficitur. Morbi dictum pretium consequat. 
								Pellentesque porta diam neque, quis posuere dolor tempor ut. Nulla pretium condimentum dignissim. Ut eleifend ex ipsum, sit amet congue 
								augue tincidunt quis. Fusce pulvinar feugiat nunc vitae tristique. Sed eget elit nibh.
							</p>
							
						</div><!-- .article-body -->
						
						<a href="#" class="button big dark">Read More</a>
					
				</div><!-- .featured -->
				<div class="aside">
				
					<div class="swiper-wrapper">
						<div class="swipe" data-links="true">
							<div class="swipe-wrap">
								<div>
									<h3 class="title">This is a News Title</h3>
									<time class="inline-time" datetime="2014-07-29">July 29, 2014</time>
									<p>
										Mauris urna est, sodales ac pulvinar in, pellentesque sed velit. Integer iaculis risus sed porta efficitur. 
										Morbi dictum pretium consequat. Pellentesque porta diam neque, quis posuere dolor tempor ut.
									</p>
									<a href="#" class="inline-read-more">Read More...</a>
								</div>
								<div>
									<h3 class="title">This is a Second News Title That is Longer</h3>
									<time class="inline-time" datetime="2014-07-29">July 29, 2014</time>
									<p>
										Mauris urna est, sodales ac pulvinar in, pellentesque sed velit. Integer iaculis risus sed porta efficitur. 
										Morbi dictum pretium consequat. Pellentesque porta diam neque, quis posuere dolor tempor ut.
									</p>
									<a href="#" class="inline-read-more">Read More...</a>
								</div>
							</div><!-- .swipe-wrap -->
						</div><!-- .swipe -->
					</div><!-- .swiper-wrapper -->
					
					<a href="#" class="button big dark">View All News</a>
					
				</div><!-- .aside -->
			</div><!-- .latest-block -->
			
		</div><!-- .sw -->
	</section><!-- .with-embellishment -->
	
	<section class="filter-area nopad">
		<div class="filter-bar with-search with-controls">
			<div class="sw">
				
				<div class="filter-controls">
					<button class="previous">Prev</button>
					<button class="next">Next</button>
				</div><!-- .filter-arrows -->
				
				<form action="/" class="single-form">
					<fieldset>
						<input type="text" name="search" placeholder="Search News...">
						<button class="fa-search" type="submit">Search</button>
					</fieldset>
				</form><!-- .single-form -->
			
				<div class="count">
					<span class="num">15</span>
					Articles Found
				</div><!-- .count -->
				
				<div class="selector with-arrow">
					<select name="sort-by">
						<option>Sort</option>
						<option value="year">Year</option>
					</select>
					<span class="value">Sort</span>
				</div><!-- .selector -->
				
			</div><!-- .sw -->
		</div><!-- .filter-bar -->
		
		<div class="filter-content">
			<div class="sw">
				<div class="grid eqh">
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">S.R. Stack Becomes Platinum Sponsor of the Manulife Relay for Life Fundraiser.</h5>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta eleifend nunc, et sagittis augue porttitor sed. 
								Sed pharetra erat non lacus venenatis, sit amet ultricies erat porttitor. Nulla ullamcorper odio ac euismod pellentesque. 
								Vestibulum eget nisl ut lorem maximus lobortis.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">A shorter title.</h5>
							
							<p>
								With hardly any content - and yet the blocks still line up reasonably well
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->
					<div class="col col-3 sm-col-2 xs-col-1">
						<a class="item article-block" href="#" >
							
							<div class="article-head">
								<time datetime="2014-03-25">
									<span class="day">25</span>
									<span class="month-year">
										<span>Mar</span>
										<span>2014</span>
									</span>
								</time>
							</div><!-- .article-head -->
							
							<h5 class="title">S.R. Stack Becomes Platinum Sponsor of the Manulife Relay for Life Fundraiser.</h5>
							
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam porta eleifend nunc, et sagittis augue porttitor sed. 
								Sed pharetra erat non lacus venenatis, sit amet ultricies erat porttitor. Nulla ullamcorper odio ac euismod pellentesque. 
								Vestibulum eget nisl ut lorem maximus lobortis.
							</p>
							
							<span class="button">Read More</span>
							
						</a><!-- .item -->
					</div><!-- .col -->

				</div><!-- .grid -->
			</div><!-- .sw -->
		</div><!-- .filter-content -->
		
	</section><!-- .filter-section -->

	
	
	<?php include('inc/i-pre-footer-modules.php'); ?>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>