<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-1.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">
	
	<article>
		<section class="dark-bg page-header">
			<div class="sw">
				<h1>Get Help</h1>
				<span class="h3-style subtitle">Phasellus interdum tempus nisi quis placerat liquam mollis</span>
			</div><!-- .sw -->
		</section><!-- .page-header -->
	
		<section>
			
			<div class="sw">
				<div class="main-body with-big-sidebar">
					<div class="content">
						<div class="article-body">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum bibendum leo, ac dignissim orci cursus at. Donec in iaculis enim. Ut nibh nisl, 
								elementum nec tortor eu, ultrices pharetra purus. Nunc aliquam lacus enim. Ut suscipit nisi vitae metus suscipit convallis. Nunc in malesuada nibh, a 
								interdum dolor. Pellentesque justo leo, posuere eget ipsum at, elementum interdum sapien.
							</p><!-- .excerpt -->
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar big-sidebar">
						<div class="contact-module">
						
							<span>Talk to us Today!</span>
						
							<div class="btn-group fa-buttons">
							
								<span class="fa-button fa fa-phone">
									<span class="block">709 722 5741</span>
								</span>
								
								<a class="fa-button fa fa-envelope-o" href="#">
									<span class="block">Email</span>
								</a>
								
								<a class="fa-button fa fa-comments-o" href="#">
									<span class="block">Live Chat</span>
								</a>
							
							</div><!-- .btn-group -->
						</div><!-- .contact-module -->
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			</div><!-- .sw -->
			
		</section>
		
		<section class="with-embellishment">
			<div class="sw">
				
				<div class="breadcrumbs">
					<a href="#">Talk to us Today</a>
				</div><!-- .breadcrumbs -->
				
				<div class="main-body with-sidebar">
					<div class="content">
						<div class="article-body">
						
							<div class="video-container full" data-ratio="37.5">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2691.215721788213!2d-52.71094900000001!3d47.583045999999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca3ef8cae57bb%3A0x50257f35bcfbd989!2sS.R.+Stack+%26+Company+Ltd.!5e0!3m2!1sen!2sca!4v1416759860149"frameborder="0" style="border:0"></iframe>
							</div><!-- .responsive-video -->

							<div class="grid pad40">
								<div class="col col-2-5 col-900-1">
									<div class="item">
									
										<address>
											S.R. Stack Inc. <br />
											84-86 Elizabeth Avenue <br />
											Regatta Plaza 2 <br />
											St. John's, NL A1A 1W7
										</address>
										
										<div class="phones rows">
										
											<span class="row">
												<span class="l">St. John's Area:</span>
												709 722 5741
											</span><!-- .block -->
											
											<span class="row">
												<span class="l">Toll Free:</span>
												1 800 285 8870
											</span><!-- .block -->
											
											<span class="row">
												<span class="l">After Hours:</span>
												709 834 4190
											</span><!-- .block -->
										</div><!-- .phones -->
										
										<br />
										
										<h5>Hours of Operation</h5>
										<hr />
										
										<div class="rows">
										
											<span class="row">
												<strong class="l">Mon - Fri</strong>
												9AM-5PM
											</span><!-- .block -->
											
											<span class="row">
												<strong class="l">Saturday</strong>
												Closed
											</span><!-- .block -->
											
											<span class="row">
												<strong class="l">Sunday</strong>
												Closed
											</span><!-- .block -->
											
										</div><!-- .phones -->
										

									
									</div><!-- .item -->
								</div><!-- .col -->
								<div class="col col-3-5 col-900-1">
									<div class="item">
									
										<form action="/" method="post" class="body-form">
											<fieldset>
												
												<input type="text" name="name" placeholder="Name">
												<input type="tel" pattern="\d+" name="phone" placeholder="Phone">
												<input type="email" name="email" placeholder="Email">
												<input type="text" name="subject" placeholder="Subject">
												<textarea name="message" placeholder="Message" cols="30" rows="10"></textarea>
												
												<button class="button" type="submit">Submit</button>
												
											</fieldset>
										</form><!-- .body-form -->
										
									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->
						
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar padded-blocks">						
						<?php include('inc/i-did-you-know-sidebar.php'); ?>
					</aside><!-- .sidebar -->
				</div><!-- .main-body.with-sidebar -->
				
			</div><!-- .sw -->
		</section><!-- .with-embellishment -->
			

	</article>
	
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>