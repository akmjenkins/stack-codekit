<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero single">
	<div class="swiper-wrapper">
		<div class="swipe">
			<div class="swipe-wrap">

				<div data-src="../assets/images/temp/hero/inner-hero-3.jpg">
					<div class="item">&nbsp;</div>					
				</div>

			</div><!-- .swipe-wrap -->
		</div><!-- .swipe -->
	</div><!-- .swiper-wrapper -->
</div><!-- .hero -->

<div class="body">

	<article>	
		<section class="dark-bg page-header">
			<div class="sw">
				<h1>Resources</h1>
				<span class="h3-style subtitle">Phasellus interdum tempus nisi quis placerat liquam mollis</span>
			</div><!-- .sw -->
		</section><!-- .page-header -->
	
		<div class="article-body">
			<section>
				
				<div class="sw">
					<div class="main-body with-big-sidebar">
						<div class="content">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur dictum bibendum leo, ac dignissim orci cursus at. Donec in iaculis enim. Ut nibh nisl, 
								elementum nec tortor eu, ultrices pharetra purus. Nunc aliquam lacus enim. Ut suscipit nisi vitae metus suscipit convallis. Nunc in malesuada nibh, a 
								interdum dolor. Pellentesque justo leo, posuere eget ipsum at, elementum interdum sapien.
							</p><!-- .excerpt -->
						</div>
						<aside class="sidebar big-sidebar">
							<div class="contact-module">
							
								<span>Talk to us Today!</span>
							
								<div class="btn-group fa-buttons">
								
									<span class="fa-button fa fa-phone">
										<span class="block">709 722 5741</span>
									</span>
									
									<a class="fa-button fa fa-envelope-o" href="#">
										<span class="block">Email</span>
									</a>
									
									<a class="fa-button fa fa-comments-o" href="#">
										<span class="block">Live Chat</span>
									</a>
								
								</div><!-- .btn-group -->
							</div><!-- .contact-module -->
						</aside><!-- .sidebar -->
					</div><!-- .main-body -->
				</div><!-- .sw.cf -->
				
			</section>
		</div><!-- .article-body -->
	</article>
	
	<section class="with-embellishment">
		<div class="sw">
		
			<div class="accordion separated allow-multiple">
				<div class="accordion-item">
					<div class="accordion-item-handle">
						Resource Category
						
						<div class="filter-arrows">
							<button class="prev fa fa-abs fa-chevron-left">Prev</button>
							<button class="next fa fa-abs fa-chevron-right">Next</button>
						</div><!-- .filter-arrows -->
					</div>
					<div class="accordion-item-content">
						
						<div class="grid eqh">
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>A Really long resource title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->
				
				<div class="accordion-item">
					<div class="accordion-item-handle">
						Resource Category
						
						<div class="filter-arrows">
							<button class="prev fa fa-abs fa-chevron-left">Prev</button>
							<button class="next fa fa-abs fa-chevron-right">Next</button>
						</div><!-- .filter-arrows -->
					</div>
					<div class="accordion-item-content">
						
						<div class="grid eqh">
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>A Really long resource title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->

				<div class="accordion-item">
					<div class="accordion-item-handle">
						Resource Category
						
						<div class="filter-arrows">
							<button class="prev fa fa-abs fa-chevron-left">Prev</button>
							<button class="next fa fa-abs fa-chevron-right">Next</button>
						</div><!-- .filter-arrows -->
					</div>
					<div class="accordion-item-content">
						
						<div class="grid eqh">
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>A Really long resource title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->

				<div class="accordion-item">
					<div class="accordion-item-handle">
						Resource Category
						
						<div class="filter-arrows">
							<button class="prev fa fa-abs fa-chevron-left">Prev</button>
							<button class="next fa fa-abs fa-chevron-right">Next</button>
						</div><!-- .filter-arrows -->
					</div>
					<div class="accordion-item-content">
						
						<div class="grid eqh">
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>A Really long resource title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->

				<div class="accordion-item">
					<div class="accordion-item-handle">
						Resource Category
						
						<div class="filter-arrows">
							<button class="prev fa fa-abs fa-chevron-left">Prev</button>
							<button class="next fa fa-abs fa-chevron-right">Next</button>
						</div><!-- .filter-arrows -->
					</div>
					<div class="accordion-item-content">
						
						<div class="grid eqh">
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>Resource Title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
							<div class="col-3 col sm-col-2 xs-col-1">
								<a class="item excerpt-block" href="#">
									
									<div class="hgroup">
										<h3>A Really long resource title</h3>
									</div>
									
									<p>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sed neque tortor. Nunc fringilla tempor nunc, id vestibulum odio venenatis non. 
										Suspendisse eget lectus ultrices, dapibus quam sed, hendrerit sapien. Nullam sed odio nec massa aliquam fringilla non at ligula
									</p>
									
									<span class="fa fa-abs fa-ellipsis-h more">Read More</span>
									
								</a><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
						
					</div><!-- .accordion-item-content -->
				</div><!-- .accordion-item -->

			</div><!--.accordion -->
		
		</div><!-- .sw -->
	</section><!-- .with-embellishment -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>