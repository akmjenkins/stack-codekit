<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<?php include('inc/i-hero-home.php'); ?>

<div class="body">
	
	<section class="dark-bg with-embellishment center">
	
		<div class="sw">
		
			<div class="section-title">
				<h2>Credit Counselling</h2>
				<span class="subtitle">If you're stressed by cash flow problems, creditors calling, or the inability to meet your debt obligations, the credit counselling professionals at S.R. Stack can help</span>
			</div><!-- .section-title -->
			
			<a href="#" class="button big">Read More</a>
		
		</div><!-- .sw -->
	</section>
	
	<section class="split-screen dark-bg center collapse-1000">
		<div class="sw">
			<div class="grid eqh collapse-1000">
				<div class="col col-2">
					<div class="item">
					
						<div class="section-title">
							<h3>Identify the Warning Signs</h3>
							<p>Get the Facts</p>
						</div><!-- .section-title -->
						
						<div class="btn-group">
						
							<a href="#" class="button">FAQs</a>
							<a href="#" class="button">Resources</a>
							<a href="#" class="button">Free Consult</a>
						
						</div><!-- .btn-group -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				<div class="col col-2">
					<div class="item">
						
							<div class="section-title">
								<h3>Get Back to Normal</h3>
								<span class="subtitle">Talk to Us Today</span>
							</div><!-- .section-title -->
							
							<div class="btn-group fa-buttons">
							
								<span class="fa-button fa fa-phone">
									<span class="block">709 722 5741</span>
								</span>
								
								<a class="fa-button fa fa-envelope-o" href="#">
									<span class="block">Email</span>
								</a>
								
								<a class="fa-button fa fa-comments-o" href="#">
									<span class="block">Live Chat</span>
								</a>
							
							</div><!-- .btn-group -->
							
					</div><!-- .item -->
				</div><!-- .col -->
			</div><!-- .grid -->
		</div><!-- .sw -->
	</section><!-- .split-screen -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>